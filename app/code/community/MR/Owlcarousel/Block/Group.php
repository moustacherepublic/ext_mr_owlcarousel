<?php

class MR_Owlcarousel_Block_Group extends Mage_Core_Block_Template {
    const GROUP_TYPE_HOME = 'home';
    const GROUP_TYPE_PAGE = 'page';

    protected $type = MR_Owlcarousel_Block_Group::GROUP_TYPE_HOME;

    protected function _construct()
    {

    }

    /**
     * Retrieve a collection of active slides of the current group
     *
     * @return MR_Owlcarousel_Model_Resource_Slide_Collection
     */

    public function getSlides($groupId) {
        $slideCollection = Mage::getModel('mr_owlcarousel/slide')->getCollection()
            ->addFieldToFilter('group_id' , $groupId)
            ->addFieldToFilter('status' , 1)
            ->setOrder('sort_order','ASC');

        return $slideCollection;
    }

    public function setType($type){
        $this->type = $type;
        return $this;
    }

     public function getType(){
         return $this->type;

     }

    public function setGroupId($group){
        $this->groupId = $group;
        return $this;
    }

    public function getGroupId(){
        return $this->groupId;

    }

}
