<?php
/**
 * MR_Owlcarousel extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       MR
 * @package        MR_Owlcarousel
 * @copyright      Copyright (c) 2014
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Slide admin edit tabs
 *
 * @category    MR
 * @package     MR_Owlcarousel
 * @author      Ultimate Module Creator
 */
class MR_Owlcarousel_Block_Adminhtml_Slide_Edit_Tabs
    extends Mage_Adminhtml_Block_Widget_Tabs {
    /**
     * Initialize Tabs
     * @access public
     * @author Ultimate Module Creator
     */
    public function __construct() {
        parent::__construct();
        $this->setId('slide_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('mr_owlcarousel')->__('Slide'));
    }
    /**
     * before render html
     * @access protected
     * @return MR_Owlcarousel_Block_Adminhtml_Slide_Edit_Tabs
     * @author Ultimate Module Creator
     */
    protected function _beforeToHtml(){
        $this->addTab('form_slide', array(
            'label'        => Mage::helper('mr_owlcarousel')->__('Slide'),
            'title'        => Mage::helper('mr_owlcarousel')->__('Slide'),
            'content'     => $this->getLayout()->createBlock('mr_owlcarousel/adminhtml_slide_edit_tab_form')->toHtml(),
        ));
        return parent::_beforeToHtml();
    }
    /**
     * Retrieve slide entity
     * @access public
     * @return MR_Owlcarousel_Model_Slide
     * @author Ultimate Module Creator
     */
    public function getSlide(){
        return Mage::registry('current_slide');
    }
}
