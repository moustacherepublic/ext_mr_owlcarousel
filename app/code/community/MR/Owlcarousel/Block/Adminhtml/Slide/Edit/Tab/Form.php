<?php
/**
 * MR_Owlcarousel extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       MR
 * @package        MR_Owlcarousel
 * @copyright      Copyright (c) 2014
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Slide edit form tab
 *
 * @category    MR
 * @package     MR_Owlcarousel
 * @author      Ultimate Module Creator
 */
class MR_Owlcarousel_Block_Adminhtml_Slide_Edit_Tab_Form
    extends Mage_Adminhtml_Block_Widget_Form {
    /**
     * prepare the form
     * @access protected
     * @return Owlcarousel_Slide_Block_Adminhtml_Slide_Edit_Tab_Form
     * @author Ultimate Module Creator
     */
    protected function _prepareForm(){
        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('slide_');
        $form->setFieldNameSuffix('slide');
        $this->setForm($form);
        $fieldset = $form->addFieldset('slide_form', array('legend'=>Mage::helper('mr_owlcarousel')->__('Slide')));
        $fieldset->addType('image', Mage::getConfig()->getBlockClassName('mr_owlcarousel/adminhtml_slide_helper_image'));
        $values = Mage::getResourceModel('mr_owlcarousel/group_collection')->toOptionArray();
        array_unshift($values, array('label'=>'', 'value'=>''));
        $fieldset->addField('group_id', 'select', array(
            'label'     => Mage::helper('mr_owlcarousel')->__('Group'),
            'name'      => 'group_id',
            'required'  => false,
            'values'    => $values
        ));

        $fieldset->addField('title', 'text', array(
            'label' => Mage::helper('mr_owlcarousel')->__('Title'),
            'name'  => 'title',
            'required'  => true,
            'class' => 'required-entry',

        ));

        $fieldset->addField('link_url', 'text', array(
            'label' => Mage::helper('mr_owlcarousel')->__('Link Url'),
            'name'  => 'link_url',

        ));

        $fieldset->addField('image', 'image', array(
            'label' => Mage::helper('mr_owlcarousel')->__('Image'),
            'name'  => 'image',

        ));

        $fieldset->addField('alt_text', 'text', array(
            'label' => Mage::helper('mr_owlcarousel')->__('Alt Text'),
            'name'  => 'alt_text',

        ));

        $fieldset->addField('sort_order', 'text', array(
            'label' => Mage::helper('mr_owlcarousel')->__('Sort Order'),
            'name'  => 'sort_order',

        ));
        $fieldset->addField('status', 'select', array(
            'label' => Mage::helper('mr_owlcarousel')->__('Status'),
            'name'  => 'status',
            'values'=> array(
                array(
                    'value' => 1,
                    'label' => Mage::helper('mr_owlcarousel')->__('Enabled'),
                ),
                array(
                    'value' => 0,
                    'label' => Mage::helper('mr_owlcarousel')->__('Disabled'),
                ),
            ),
        ));
        $formValues = Mage::registry('current_slide')->getDefaultValues();
        if (!is_array($formValues)){
            $formValues = array();
        }
        if (Mage::getSingleton('adminhtml/session')->getSlideData()){
            $formValues = array_merge($formValues, Mage::getSingleton('adminhtml/session')->getSlideData());
            Mage::getSingleton('adminhtml/session')->setSlideData(null);
        }
        elseif (Mage::registry('current_slide')){
            $formValues = array_merge($formValues, Mage::registry('current_slide')->getData());
        }
        $form->setValues($formValues);
        return parent::_prepareForm();
    }
}
