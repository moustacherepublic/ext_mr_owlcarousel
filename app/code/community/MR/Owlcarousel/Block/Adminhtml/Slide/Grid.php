<?php
/**
 * MR_Owlcarousel extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       MR
 * @package        MR_Owlcarousel
 * @copyright      Copyright (c) 2014
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Slide admin grid block
 *
 * @category    MR
 * @package     MR_Owlcarousel
 * @author      Ultimate Module Creator
 */
class MR_Owlcarousel_Block_Adminhtml_Slide_Grid
    extends Mage_Adminhtml_Block_Widget_Grid {
    /**
     * constructor
     * @access public
     * @author Ultimate Module Creator
     */
    public function __construct(){
        parent::__construct();
        $this->setId('slideGrid');
        $this->setDefaultSort('entity_id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }
    /**
     * prepare collection
     * @access protected
     * @return MR_Owlcarousel_Block_Adminhtml_Slide_Grid
     * @author Ultimate Module Creator
     */
    protected function _prepareCollection(){
        $collection = Mage::getModel('mr_owlcarousel/slide')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }
    /**
     * prepare grid collection
     * @access protected
     * @return MR_Owlcarousel_Block_Adminhtml_Slide_Grid
     * @author Ultimate Module Creator
     */
    protected function _prepareColumns(){
        $this->addColumn('entity_id', array(
            'header'    => Mage::helper('mr_owlcarousel')->__('Id'),
            'index'        => 'entity_id',
            'type'        => 'number'
        ));
        $this->addColumn('group_id', array(
            'header'    => Mage::helper('mr_owlcarousel')->__('Group'),
            'index'     => 'group_id',
            'type'      => 'options',
            'options'   => Mage::getResourceModel('mr_owlcarousel/group_collection')->toOptionHash(),
        ));
        $this->addColumn('title', array(
            'header'    => Mage::helper('mr_owlcarousel')->__('Title'),
            'align'     => 'left',
            'index'     => 'title',
        ));
        $this->addColumn('status', array(
            'header'    => Mage::helper('mr_owlcarousel')->__('Status'),
            'index'        => 'status',
            'type'        => 'options',
            'options'    => array(
                '1' => Mage::helper('mr_owlcarousel')->__('Enabled'),
                '0' => Mage::helper('mr_owlcarousel')->__('Disabled'),
            )
        ));
        $this->addColumn('link_url', array(
            'header'=> Mage::helper('mr_owlcarousel')->__('Link Url'),
            'index' => 'link_url',
            'type'=> 'text',

        ));
        $this->addColumn('alt_text', array(
            'header'=> Mage::helper('mr_owlcarousel')->__('Alt Text'),
            'index' => 'alt_text',
            'type'=> 'text',

        ));
        $this->addColumn('sort_order', array(
            'header'=> Mage::helper('mr_owlcarousel')->__('Sort Order'),
            'index' => 'sort_order',
            'type'=> 'number',

        ));
        $this->addColumn('created_at', array(
            'header'    => Mage::helper('mr_owlcarousel')->__('Created at'),
            'index'     => 'created_at',
            'width'     => '120px',
            'type'      => 'datetime',
        ));
        $this->addColumn('updated_at', array(
            'header'    => Mage::helper('mr_owlcarousel')->__('Updated at'),
            'index'     => 'updated_at',
            'width'     => '120px',
            'type'      => 'datetime',
        ));
        $this->addColumn('action',
            array(
                'header'=>  Mage::helper('mr_owlcarousel')->__('Action'),
                'width' => '100',
                'type'  => 'action',
                'getter'=> 'getId',
                'actions'   => array(
                    array(
                        'caption'   => Mage::helper('mr_owlcarousel')->__('Edit'),
                        'url'   => array('base'=> '*/*/edit'),
                        'field' => 'id'
                    )
                ),
                'filter'=> false,
                'is_system'    => true,
                'sortable'  => false,
        ));
        $this->addExportType('*/*/exportCsv', Mage::helper('mr_owlcarousel')->__('CSV'));
        $this->addExportType('*/*/exportExcel', Mage::helper('mr_owlcarousel')->__('Excel'));
        $this->addExportType('*/*/exportXml', Mage::helper('mr_owlcarousel')->__('XML'));
        return parent::_prepareColumns();
    }
    /**
     * prepare mass action
     * @access protected
     * @return MR_Owlcarousel_Block_Adminhtml_Slide_Grid
     * @author Ultimate Module Creator
     */
    protected function _prepareMassaction(){
        $this->setMassactionIdField('entity_id');
        $this->getMassactionBlock()->setFormFieldName('slide');
        $this->getMassactionBlock()->addItem('delete', array(
            'label'=> Mage::helper('mr_owlcarousel')->__('Delete'),
            'url'  => $this->getUrl('*/*/massDelete'),
            'confirm'  => Mage::helper('mr_owlcarousel')->__('Are you sure?')
        ));
        $this->getMassactionBlock()->addItem('status', array(
            'label'=> Mage::helper('mr_owlcarousel')->__('Change status'),
            'url'  => $this->getUrl('*/*/massStatus', array('_current'=>true)),
            'additional' => array(
                'status' => array(
                        'name' => 'status',
                        'type' => 'select',
                        'class' => 'required-entry',
                        'label' => Mage::helper('mr_owlcarousel')->__('Status'),
                        'values' => array(
                                '1' => Mage::helper('mr_owlcarousel')->__('Enabled'),
                                '0' => Mage::helper('mr_owlcarousel')->__('Disabled'),
                        )
                )
            )
        ));
        $values = Mage::getResourceModel('mr_owlcarousel/group_collection')->toOptionHash();
        $values = array_reverse($values, true);
        $values[''] = '';
        $values = array_reverse($values, true);
        $this->getMassactionBlock()->addItem('group_id', array(
            'label'=> Mage::helper('mr_owlcarousel')->__('Change Group'),
            'url'  => $this->getUrl('*/*/massGroupId', array('_current'=>true)),
            'additional' => array(
                'flag_group_id' => array(
                        'name' => 'flag_group_id',
                        'type' => 'select',
                        'class' => 'required-entry',
                        'label' => Mage::helper('mr_owlcarousel')->__('Group'),
                        'values' => $values
                )
            )
        ));
        return $this;
    }
    /**
     * get the row url
     * @access public
     * @param MR_Owlcarousel_Model_Slide
     * @return string
     * @author Ultimate Module Creator
     */
    public function getRowUrl($row){
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }
    /**
     * get the grid url
     * @access public
     * @return string
     * @author Ultimate Module Creator
     */
    public function getGridUrl(){
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }
    /**
     * after collection load
     * @access protected
     * @return MR_Owlcarousel_Block_Adminhtml_Slide_Grid
     * @author Ultimate Module Creator
     */
    protected function _afterLoadCollection(){
        $this->getCollection()->walk('afterLoad');
        parent::_afterLoadCollection();
    }
}
