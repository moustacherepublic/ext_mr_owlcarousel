<?php
/**
 * MR_Owlcarousel extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       MR
 * @package        MR_Owlcarousel
 * @copyright      Copyright (c) 2014
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Group admin block
 *
 * @category    MR
 * @package     MR_Owlcarousel
 * @author      Ultimate Module Creator
 */
class MR_Owlcarousel_Block_Adminhtml_Group
    extends Mage_Adminhtml_Block_Widget_Grid_Container {
    /**
     * constructor
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function __construct(){
        $this->_controller         = 'adminhtml_group';
        $this->_blockGroup         = 'mr_owlcarousel';
        parent::__construct();
        $this->_headerText         = Mage::helper('mr_owlcarousel')->__('Manage Groups');
        $this->_updateButton('add', 'label', Mage::helper('mr_owlcarousel')->__('Add Group'));

    }
}
