<?php
/**
 * MR_Owlcarousel extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       MR
 * @package        MR_Owlcarousel
 * @copyright      Copyright (c) 2014
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Group admin edit form
 *
 * @category    MR
 * @package     MR_Owlcarousel
 * @author      Ultimate Module Creator
 */
class MR_Owlcarousel_Block_Adminhtml_Group_Edit
    extends Mage_Adminhtml_Block_Widget_Form_Container {
    /**
     * constructor
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function __construct(){
        parent::__construct();
        $this->_blockGroup = 'mr_owlcarousel';
        $this->_controller = 'adminhtml_group';
        $this->_updateButton('save', 'label', Mage::helper('mr_owlcarousel')->__('Save Group'));
        $this->_updateButton('delete', 'label', Mage::helper('mr_owlcarousel')->__('Delete Group'));
        $this->_addButton('saveandcontinue', array(
            'label'        => Mage::helper('mr_owlcarousel')->__('Save And Continue Edit'),
            'onclick'    => 'saveAndContinueEdit()',
            'class'        => 'save',
        ), -100);
        $this->_formScripts[] = "
            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }
    /**
     * get the edit form header
     * @access public
     * @return string
     * @author Ultimate Module Creator
     */
    public function getHeaderText(){
        if( Mage::registry('current_group') && Mage::registry('current_group')->getId() ) {
            return Mage::helper('mr_owlcarousel')->__("Edit Group '%s'", $this->escapeHtml(Mage::registry('current_group')->getTitle()));
        }
        else {
            return Mage::helper('mr_owlcarousel')->__('Add Group');
        }
    }
}
