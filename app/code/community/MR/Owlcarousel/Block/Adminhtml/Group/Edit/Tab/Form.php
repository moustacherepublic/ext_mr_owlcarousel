<?php
/**
 * MR_Owlcarousel extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 *
 * @category       MR
 * @package        MR_Owlcarousel
 * @copyright      Copyright (c) 2014
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Group edit form tab
 *
 * @category    MR
 * @package     MR_Owlcarousel
 * @author      Ultimate Module Creator
 */
class MR_Owlcarousel_Block_Adminhtml_Group_Edit_Tab_Form
    extends Mage_Adminhtml_Block_Widget_Form {
    /**
     * prepare the form
     * @access protected
     * @return Owlcarousel_Group_Block_Adminhtml_Group_Edit_Tab_Form
     * @author Ultimate Module Creator
     */
    protected function _prepareForm(){
        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('group_');
        $form->setFieldNameSuffix('group');
        $this->setForm($form);
        $fieldset = $form->addFieldset('group_form', array('legend'=>Mage::helper('mr_owlcarousel')->__('Group')));

        $fieldset->addField('title', 'text', array(
            'label' => Mage::helper('mr_owlcarousel')->__('Title'),
            'name'  => 'title',
            'required'  => true,
            'class' => 'required-entry',

        ));
        $fieldset->addField('type', 'select', array(
            'label' => Mage::helper('mr_owlcarousel')->__('Type'),
            'name'  => 'type',
            'required'  => true,
            'class' => 'required-entry',
            'values'=> array(
                array(
                    'value' => MR_Owlcarousel_Block_Group::GROUP_TYPE_HOME,
                    'label' => Mage::helper('mr_owlcarousel')->__('Home Page'),
                ),
                array(
                    'value' => MR_Owlcarousel_Block_Group::GROUP_TYPE_PAGE,
                    'label' => Mage::helper('mr_owlcarousel')->__('CMS Page'),
                ),
            ),

        ));
        $fieldset->addField('status', 'select', array(
            'label' => Mage::helper('mr_owlcarousel')->__('Status'),
            'name'  => 'status',
            'values'=> array(
                array(
                    'value' => 1,
                    'label' => Mage::helper('mr_owlcarousel')->__('Enabled'),
                ),
                array(
                    'value' => 0,
                    'label' => Mage::helper('mr_owlcarousel')->__('Disabled'),
                ),
            ),
        ));
        /**
         * Check is single store mode
         */
        if (!Mage::app()->isSingleStoreMode()) {
            $field =$fieldset->addField('store_id', 'multiselect', array(
                'name'      => 'stores[]',
                'label'     => Mage::helper('mr_owlcarousel')->__('Store View'),
                'title'     => Mage::helper('mr_owlcarousel')->__('Store View'),
                'required'  => true,
                'values'    => Mage::getSingleton('adminhtml/system_store')->getStoreValuesForForm(false, true),
            ));
            $renderer = $this->getLayout()->createBlock('adminhtml/store_switcher_form_renderer_fieldset_element');
            $field->setRenderer($renderer);
        }
        else {
            $fieldset->addField('store_id', 'hidden', array(
                'name'      => 'stores[]',
                'value'     => Mage::app()->getStore(true)->getId()
            ));
            Mage::registry('current_group')->setStoreId(Mage::app()->getStore(true)->getId());
        }
        $formValues = Mage::registry('current_group')->getDefaultValues();
        if (!is_array($formValues)){
            $formValues = array();
        }
        if (Mage::getSingleton('adminhtml/session')->getGroupData()){
            $formValues = array_merge($formValues, Mage::getSingleton('adminhtml/session')->getGroupData());
            Mage::getSingleton('adminhtml/session')->setGroupData(null);
        }
        elseif (Mage::registry('current_group')){
            $formValues = array_merge($formValues, Mage::registry('current_group')->getData());
        }
        $form->setValues($formValues);
        return parent::_prepareForm();
    }
}
