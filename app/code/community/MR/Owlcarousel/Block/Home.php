<?php

class MR_Owlcarousel_Block_Home extends MR_Owlcarousel_Block_Group
{
    protected function _toHtml()
    {
        $html = '';
        $groups = $this->getGroupsByStore();
        foreach ($groups as $group) {
            $groupId = $group->getId();
            $groupBlock = $this->getLayout()->createBlock('mr_owlcarousel/group')
                ->setCacheKey(MR_Owlcarousel_Model_Group::CACHE_TAG . '_' . Mage::app()->getStore()->getId() . '_' . $groupId)
                ->setCacheLifetime(86400)
                ->addCacheTag(array(MR_Owlcarousel_Model_Group::CACHE_TAG . '_' . $groupId))
                ->setTemplate('mr/owl-carousel/group.phtml')
                ->setGroupId($groupId);

            $html .= $groupBlock->toHtml();
        }
        return $html;

    }

    public function getGroupsByStore()
    {
        $groupCollection = Mage::getModel('mr_owlcarousel/group')->getCollection()
            ->addStoreFilter(Mage::app()->getStore()->getStoreId())
            ->addFieldToFilter('status', 1)
            ->addFieldToFilter('type',array(MR_Owlcarousel_Block_Group::GROUP_TYPE_HOME,array('null' => true)));

        return $groupCollection;
    }

}
