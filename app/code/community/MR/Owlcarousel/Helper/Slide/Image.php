<?php 
/**
 * MR_Owlcarousel extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       MR
 * @package        MR_Owlcarousel
 * @copyright      Copyright (c) 2014
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Slide image helper
 *
 * @category    MR
 * @package     MR_Owlcarousel
 * @author      Ultimate Module Creator
 */
class MR_Owlcarousel_Helper_Slide_Image
    extends MR_Owlcarousel_Helper_Image_Abstract {
    /**
     * image placeholder
     * @var string
     */
    protected $_placeholder = 'images/placeholder/slide.jpg';
    /**
     * image subdir
     * @var string
     */
    protected $_subdir      = 'slide';
}
