<?php
class MR_Owlcarousel_Model_Mysql4_Group extends Mage_Core_Model_Mysql4_Abstract
{
    protected function _construct()
    {
        $this->_init("owlcarousel/group", "group_id");
    }
}