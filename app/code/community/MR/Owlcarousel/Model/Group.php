<?php
/**
 * MR_Owlcarousel extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 *
 * @category       MR
 * @package        MR_Owlcarousel
 * @copyright      Copyright (c) 2014
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Group model
 *
 * @category    MR
 * @package     MR_Owlcarousel
 * @author      Ultimate Module Creator
 */
class MR_Owlcarousel_Model_Group
    extends Mage_Core_Model_Abstract {
    /**
     * Entity code.
     * Can be used as part of method name for entity processing
     */
    const ENTITY    = 'mr_owlcarousel_group';
    const CACHE_TAG = 'mr_owlcarousel_group';
    /**
     * Prefix of model events names
     * @var string
     */
    protected $_eventPrefix = 'mr_owlcarousel_group';

    /**
     * Parameter name in event
     * @var string
     */
    protected $_eventObject = 'group';
    /**
     * constructor
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function _construct(){
        parent::_construct();
        $this->_init('mr_owlcarousel/group');
    }
    /**
     * before save group
     * @access protected
     * @return MR_Owlcarousel_Model_Group
     * @author Ultimate Module Creator
     */
    protected function _beforeSave(){

        parent::_beforeSave();
        $now = Mage::getSingleton('core/date')->gmtDate();
        if ($this->isObjectNew()){
            $this->setCreatedAt($now);
        }
        $this->setUpdatedAt($now);
        return $this;
    }

    protected function _afterDelete()
    {
        $this->cleanCache();
        return parent::_afterDelete();
    }

    /**
     * save group relation
     * @access public
     * @return MR_Owlcarousel_Model_Group
     * @author Ultimate Module Creator
     */
    protected function _afterSave() {
        $this->cleanCache();
        return parent::_afterSave();
    }
    /**
     * Retrieve  collection
     * @access public
     * @return MR_Owlcarousel_Model_Slide_Collection
     * @author Ultimate Module Creator
     */
    public function getSelectedSlidesCollection(){
        if (!$this->hasData('_slide_collection')) {
            if (!$this->getId()) {
                return new Varien_Data_Collection();
            }
            else {
                $collection = Mage::getResourceModel('mr_owlcarousel/slide_collection')
                        ->addFieldToFilter('group_id', $this->getId());
                $this->setData('_slide_collection', $collection);
            }
        }
        return $this->getData('_slide_collection');
    }
    /**
     * get default values
     * @access public
     * @return array
     * @author Ultimate Module Creator
     */
    public function getDefaultValues() {
        $values = array();
        $values['status'] = 1;
        return $values;
    }

    /**
     * Clear cache related with group id
     *
     * @return MR_Owlcarousel_Model_Group
     */
    public function cleanCache()
    {
        $groupId = $this->getId();
        Mage::app()->cleanCache(MR_Owlcarousel_Model_Group::CACHE_TAG .'_'.$groupId);
        return $this;
    }
}
