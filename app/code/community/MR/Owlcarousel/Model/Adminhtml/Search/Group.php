<?php
/**
 * MR_Owlcarousel extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       MR
 * @package        MR_Owlcarousel
 * @copyright      Copyright (c) 2014
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Admin search model
 *
 * @category    MR
 * @package     MR_Owlcarousel
 * @author      Ultimate Module Creator
 */
class MR_Owlcarousel_Model_Adminhtml_Search_Group
    extends Varien_Object {
    /**
     * Load search results
     * @access public
     * @return MR_Owlcarousel_Model_Adminhtml_Search_Group
     * @author Ultimate Module Creator
     */
    public function load(){
        $arr = array();
        if (!$this->hasStart() || !$this->hasLimit() || !$this->hasQuery()) {
            $this->setResults($arr);
            return $this;
        }
        $collection = Mage::getResourceModel('mr_owlcarousel/group_collection')
            ->addFieldToFilter('title', array('like' => $this->getQuery().'%'))
            ->setCurPage($this->getStart())
            ->setPageSize($this->getLimit())
            ->load();
        foreach ($collection->getItems() as $group) {
            $arr[] = array(
                'id'=> 'group/1/'.$group->getId(),
                'type'  => Mage::helper('mr_owlcarousel')->__('Group'),
                'name'  => $group->getTitle(),
                'description'   => $group->getTitle(),
                'url' => Mage::helper('adminhtml')->getUrl('*/owlcarousel_group/edit', array('id'=>$group->getId())),
            );
        }
        $this->setResults($arr);
        return $this;
    }
}
