<?php
/**
 * MR_Owlcarousel extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 *
 * @category       MR
 * @package        MR_Owlcarousel
 * @copyright      Copyright (c) 2014
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Slide model
 *
 * @category    MR
 * @package     MR_Owlcarousel
 * @author      Ultimate Module Creator
 */
class MR_Owlcarousel_Model_Slide
    extends Mage_Core_Model_Abstract {
    /**
     * Entity code.
     * Can be used as part of method name for entity processing
     */
    const ENTITY    = 'mr_owlcarousel_slide';
    const CACHE_TAG = 'mr_owlcarousel_slide';
    /**
     * Prefix of model events names
     * @var string
     */
    protected $_eventPrefix = 'mr_owlcarousel_slide';

    /**
     * Parameter name in event
     * @var string
     */
    protected $_eventObject = 'slide';
    /**
     * constructor
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function _construct(){
        parent::_construct();
        $this->_init('mr_owlcarousel/slide');
    }
    /**
     * before save slide
     * @access protected
     * @return MR_Owlcarousel_Model_Slide
     * @author Ultimate Module Creator
     */
    protected function _beforeSave(){
        parent::_beforeSave();
        $now = Mage::getSingleton('core/date')->gmtDate();
        if ($this->isObjectNew()){
            $this->setCreatedAt($now);
        }
        $this->setUpdatedAt($now);
        return $this;
    }

    protected function _afterDelete()
    {
        $this->cleanCache();
        return parent::_afterDelete();
    }

    /**
     * save slide relation
     * @access public
     * @return MR_Owlcarousel_Model_Slide
     * @author Ultimate Module Creator
     */
    protected function _afterSave() {
        $this->cleanCache();
        return parent::_afterSave();
    }
    /**
     * Retrieve parent
     * @access public
     * @return null|MR_Owlcarousel_Model_Group
     * @author Ultimate Module Creator
     */
    public function getParentGroup(){
        if (!$this->hasData('_parent_group')) {
            if (!$this->getGroupId()) {
                return null;
            }
            else {
                $group = Mage::getModel('mr_owlcarousel/group')->load($this->getGroupId());
                if ($group->getId()) {
                    $this->setData('_parent_group', $group);
                }
                else {
                    $this->setData('_parent_group', null);
                }
            }
        }
        return $this->getData('_parent_group');
    }
    /**
     * get default values
     * @access public
     * @return array
     * @author Ultimate Module Creator
     */
    public function getDefaultValues() {
        $values = array();
        $values['status'] = 1;
        return $values;
    }

    public function getImageUrl(){
        return Mage::helper('mr_owlcarousel/slide_image')->getImageBaseUrl() . $this->getImage();
    }

    public function getSlideLinkUrl(){
        $linkUrl = $this->getLinkUrl();
        if($linkUrl){
            //add http:// to url
            if(strpos($linkUrl,'http') === false){
                $linkUrl = 'http://' . $linkUrl;
            }

            return $linkUrl;
        }else{
            return false;
        }
    }

    /**
     * Clear cache related with group id
     *
     * @return MR_Owlcarousel_Model_Slide
     */
    public function cleanCache()
    {
        $groupId = $this->getGroupId();
        $oldGroupId = $this->getOrigData('group_id');
        if($oldGroupId && ($groupId != $oldGroupId)){
        Mage::app()->cleanCache(MR_Owlcarousel_Model_Group::CACHE_TAG .'_'.$oldGroupId);
        }
        Mage::app()->cleanCache(MR_Owlcarousel_Model_Group::CACHE_TAG .'_'.$groupId);
        return $this;
    }
}
