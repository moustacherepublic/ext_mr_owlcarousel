<?php
/**
 * MR_Owlcarousel extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       MR
 * @package        MR_Owlcarousel
 * @copyright      Copyright (c) 2014
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Group admin controller
 *
 * @category    MR
 * @package     MR_Owlcarousel
 * @author      Ultimate Module Creator
 */
class MR_Owlcarousel_Adminhtml_Owlcarousel_GroupController
    extends MR_Owlcarousel_Controller_Adminhtml_Owlcarousel {
    /**
     * init the group
     * @access protected
     * @return MR_Owlcarousel_Model_Group
     */
    protected function _initGroup(){
        $groupId  = (int) $this->getRequest()->getParam('id');
        $group    = Mage::getModel('mr_owlcarousel/group');
        if ($groupId) {
            $group->load($groupId);
        }
        Mage::register('current_group', $group);
        return $group;
    }
     /**
     * default action
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function indexAction() {
        $this->loadLayout();
        $this->_title(Mage::helper('mr_owlcarousel')->__('Slider'))
             ->_title(Mage::helper('mr_owlcarousel')->__('Groups'));
        $this->renderLayout();
    }
    /**
     * grid action
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function gridAction() {
        $this->loadLayout()->renderLayout();
    }
    /**
     * edit group - action
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function editAction() {
        $groupId    = $this->getRequest()->getParam('id');
        $group      = $this->_initGroup();
        if ($groupId && !$group->getId()) {
            $this->_getSession()->addError(Mage::helper('mr_owlcarousel')->__('This group no longer exists.'));
            $this->_redirect('*/*/');
            return;
        }
        $data = Mage::getSingleton('adminhtml/session')->getGroupData(true);
        if (!empty($data)) {
            $group->setData($data);
        }
        Mage::register('group_data', $group);
        $this->loadLayout();
        $this->_title(Mage::helper('mr_owlcarousel')->__('Slider'))
             ->_title(Mage::helper('mr_owlcarousel')->__('Groups'));
        if ($group->getId()){
            $this->_title($group->getTitle());
        }
        else{
            $this->_title(Mage::helper('mr_owlcarousel')->__('Add group'));
        }
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
        }
        $this->renderLayout();
    }
    /**
     * new group action
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function newAction() {
        $this->_forward('edit');
    }
    /**
     * save group - action
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function saveAction() {
        if ($data = $this->getRequest()->getPost('group')) {
            try {
                $group = $this->_initGroup();
                $group->addData($data);
                $group->save();
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('mr_owlcarousel')->__('Group was successfully saved'));
                Mage::getSingleton('adminhtml/session')->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $group->getId()));
                    return;
                }
                $this->_redirect('*/*/');
                return;
            }
            catch (Mage_Core_Exception $e){
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setGroupData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
            catch (Exception $e) {
                Mage::logException($e);
                Mage::getSingleton('adminhtml/session')->addError(Mage::helper('mr_owlcarousel')->__('There was a problem saving the group.'));
                Mage::getSingleton('adminhtml/session')->setGroupData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('mr_owlcarousel')->__('Unable to find group to save.'));
        $this->_redirect('*/*/');
    }
    /**
     * delete group - action
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function deleteAction() {
        if( $this->getRequest()->getParam('id') > 0) {
            try {
                $group = Mage::getModel('mr_owlcarousel/group');
                $group->load($this->getRequest()->getParam('id'))->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('mr_owlcarousel')->__('Group was successfully deleted.'));
                $this->_redirect('*/*/');
                return;
            }
            catch (Mage_Core_Exception $e){
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            }
            catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(Mage::helper('mr_owlcarousel')->__('There was an error deleting group.'));
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                Mage::logException($e);
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('mr_owlcarousel')->__('Could not find group to delete.'));
        $this->_redirect('*/*/');
    }
    /**
     * mass delete group - action
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function massDeleteAction() {
        $groupIds = $this->getRequest()->getParam('group');
        if(!is_array($groupIds)) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('mr_owlcarousel')->__('Please select groups to delete.'));
        }
        else {
            try {
                foreach ($groupIds as $groupId) {
                    $group = Mage::getModel('mr_owlcarousel/group');
                    $group->load($groupId)->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('mr_owlcarousel')->__('Total of %d groups were successfully deleted.', count($groupIds)));
            }
            catch (Mage_Core_Exception $e){
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
            catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(Mage::helper('mr_owlcarousel')->__('There was an error deleting groups.'));
                Mage::logException($e);
            }
        }
        $this->_redirect('*/*/index');
    }
    /**
     * mass status change - action
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function massStatusAction(){
        $groupIds = $this->getRequest()->getParam('group');
        if(!is_array($groupIds)) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('mr_owlcarousel')->__('Please select groups.'));
        }
        else {
            try {
                foreach ($groupIds as $groupId) {
                $group = Mage::getSingleton('mr_owlcarousel/group')->load($groupId)
                            ->setStatus($this->getRequest()->getParam('status'))
                            ->setIsMassupdate(true)
                            ->save();
                }
                $this->_getSession()->addSuccess($this->__('Total of %d groups were successfully updated.', count($groupIds)));
            }
            catch (Mage_Core_Exception $e){
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
            catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(Mage::helper('mr_owlcarousel')->__('There was an error updating groups.'));
                Mage::logException($e);
            }
        }
        $this->_redirect('*/*/index');
    }
    /**
     * export as csv - action
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function exportCsvAction(){
        $fileName   = 'group.csv';
        $content    = $this->getLayout()->createBlock('mr_owlcarousel/adminhtml_group_grid')->getCsv();
        $this->_prepareDownloadResponse($fileName, $content);
    }
    /**
     * export as MsExcel - action
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function exportExcelAction(){
        $fileName   = 'group.xls';
        $content    = $this->getLayout()->createBlock('mr_owlcarousel/adminhtml_group_grid')->getExcelFile();
        $this->_prepareDownloadResponse($fileName, $content);
    }
    /**
     * export as xml - action
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function exportXmlAction(){
        $fileName   = 'group.xml';
        $content    = $this->getLayout()->createBlock('mr_owlcarousel/adminhtml_group_grid')->getXml();
        $this->_prepareDownloadResponse($fileName, $content);
    }
    /**
     * Check if admin has permissions to visit related pages
     * @access protected
     * @return boolean
     * @author Ultimate Module Creator
     */
    protected function _isAllowed() {
        return Mage::getSingleton('admin/session')->isAllowed('cms/mr_owlcarousel/group');
    }
}
