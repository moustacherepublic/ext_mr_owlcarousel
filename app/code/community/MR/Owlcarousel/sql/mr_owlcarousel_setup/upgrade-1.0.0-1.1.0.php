<?php
/**
 * MR_Owlcarousel extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 *
 * @category       MR
 * @package        MR_Owlcarousel
 * @copyright      Copyright (c) 2014
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Owlcarousel module install script
 *
 * @category    MR
 * @package     MR_Owlcarousel
 * @author      Ultimate Module Creator
 */
$this->startSetup();
$this->getConnection()
    ->addColumn($this->getTable('mr_owlcarousel/group'), 'type', array(
        'type'    => Varien_Db_Ddl_Table::TYPE_TEXT,
        'comment' => 'Slider group type',
        'nullable'  => true
    ));
$this->endSetup();
